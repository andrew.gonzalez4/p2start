package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {
	
	private static HashTableOA<Long, Car> cMap;
	private static CarTable Singleton = new CarTable();
	
	private CarTable() {
		cMap =  new HashTableOA<Long,Car>(10, new LongComparator(), new CarComparator());
	}
	
	public static HashTableOA<Long, Car> getInstance(){
		return cMap;
	}
	
	public static void resetCars(){
		cMap = new HashTableOA<Long,Car>(10, new LongComparator(), new CarComparator());
	}
	
}
