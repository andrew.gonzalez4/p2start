package edu.uprm.cse.datastructures.cardealer.util;

import java.awt.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class HashTableOA<K, V> implements Map<K, V> {

	public static class MapEntry<K,V> {

		private K key;
		private V value;
		private boolean state;

		public K getKey() {
			return key;
		}
		public void setKey(K k) {
			this.key = k;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V v) {
			this.value = v;
		}
		public boolean getState() {
			return state;
		}
		public void setState(boolean state) {
			this.state = state;
		}
		public MapEntry(K k, V v, boolean state) {
			super();
			this.key = k;
			this.value = v;
			this.state = state;
		}
	}

	private int currentSize;
	private Object[] buckets;
	private Comparator<K> keyComp;
	private Comparator<V> valueComp;
	private boolean isOcuppied;
	private static final int  INITIAL_BUCKETS = 10;

	private int HashFunction(K key, int size) {
		return Math.abs((key.hashCode()*7)% size);
	}

	private int HashFunctionSecondary(K key, int size) {
		return Math.abs(((key.hashCode())^2*7)% size);
	}

	public HashTableOA(int numBuckets, Comparator cmpKeys, Comparator cmpValues) {
		this.buckets = new Object[numBuckets];
		this.keyComp = cmpKeys;
		this.valueComp = cmpValues;
		for (int i = 0; i < buckets.length; i++) {
			this.buckets[i] = new MapEntry<K,V>(null, null, false);
		}
	}

	public HashTableOA() {
		this(INITIAL_BUCKETS, null, null);
	}


	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		if(this.currentSize == 0) return true;

		else return false;
	}

	public void makeEmpty() {
		for (int i = 0; i < this.buckets.length; i++) {
			this.buckets[i] = null;
		}
		this.currentSize = 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K k) {
		//If the key given is null don't return anything.
		if(k == null) return null;

		//First hashing.
		int desiredPosition = this.HashFunction(k, this.buckets.length);

		if(((MapEntry<K,V>) this.buckets[desiredPosition]).getState() != false) {


			//Check if the first hashing key matches the desired.
			if(((MapEntry<K,V>) this.buckets[desiredPosition]).getKey().equals(k)) {
				return ((MapEntry<K, V>) this.buckets[desiredPosition]).getValue();
			}

			//If it doesn't match, check the second hash.
			//CHECK THIS CONDITION
			else {

				//Second hashing.
				int newPos = this.HashFunctionSecondary(k,this.buckets.length);

				//If the second hashing matches, return the value of the key.
				if(((MapEntry<K, V>) this.buckets[newPos]).getKey() == k) {
					return ((MapEntry<K, V>) this.buckets[newPos]).getValue();
				}

				//If it doesn't match, do linear probing.
				else {
					for (int i = 0; i < buckets.length; i++) {
						//Applied linear probing.
						int position = (desiredPosition + i) % this.buckets.length;
						if(((MapEntry<K, V>) this.buckets[position]).getKey() == k) {
							return ((MapEntry<K, V>) this.buckets[position]).getValue();
						}
					}
				}
			}
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public void reAllocate() {

		//Temp to store the old values.
		Object[] temp = this.buckets;
		
		//Now double the size of the old array and reset it.
		this.buckets = new Object[this.buckets.length *2];
		for (int i = 0; i < this.buckets.length; i++) {
			this.buckets[i] = new MapEntry<K,V>(null, null, false);
		}
		this.currentSize = 0;
		//Add the elements using the put method :)
		for (int i = 0; i < temp.length; i++) {
			if(temp[i] == null) {
				continue;
			}
			if(((MapEntry<K, V>) temp[i]).getState() != false) {
				this.put(((MapEntry<K, V>) temp[i]).getKey(), ((MapEntry<K, V>) temp[i]).getValue());
			}
		}
		
	
	}

	@SuppressWarnings("unchecked")
	@Override
	public V put(K k, V v) {

		//If the key or value are null, don't add them to the buckets.
		if(k == null || v == null) {
			return null;
		}

		V value = v;

		if(this.contains(k) == true) {
			value = this.get(k);
			this.remove(k);
		}

		if(this.currentSize >= this.buckets.length/2) {
			reAllocate();
		}

		//First hashing 
		int desiredPosition  = this.HashFunction(k, this.buckets.length);

		if(((MapEntry<K, V>) this.buckets[desiredPosition]).getState() == false) {
			//If the bucket is empty, set the element and the key of the bucket to the one assigned.
			if(((MapEntry<K, V>) this.buckets[desiredPosition]).getValue() == null) {
				((MapEntry<K, V>) this.buckets[desiredPosition]).setKey(k);
				((MapEntry<K, V>) this.buckets[desiredPosition]).setValue(v);
				((MapEntry<K, V>) this.buckets[desiredPosition]).setState(true);
				this.currentSize++;
				return value;
			}


			//If the bucket wasn't empty, make the new hashing function.
			else {
				//Second hashing.
				int newPos = this.HashFunctionSecondary(k, this.buckets.length);

				//If the bucket is now empty, add it.
				if(((MapEntry<K, V>) this.buckets[newPos]).getValue() == null) {
					((MapEntry<K, V>) this.buckets[newPos]).setKey(k);
					((MapEntry<K, V>) this.buckets[newPos]).setValue(v);
					((MapEntry<K, V>) this.buckets[desiredPosition]).setState(true);
					this.currentSize++;
					return value;
				}

				//If the bucket was not empty, proceed to linear probing.
				else {
					//Probing
					for (int i = 0; i < buckets.length; i++) {
						//Applied linear probing.
						int position = (desiredPosition + i) % this.buckets.length;
						if(((MapEntry<K, V>) this.buckets[position]).getValue() == null) {
							((MapEntry<K, V>) this.buckets[position]).setKey(k);
							((MapEntry<K, V>) this.buckets[position]).setValue(v);
							((MapEntry<K, V>) this.buckets[desiredPosition]).setState(true);
							this.currentSize++;
							return value;
						}
					}
				}
			}
		}
		return value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(K k) {

		if(k == null) {
			return null;
		}
		int apparentPosition = this.HashFunction(k, this.buckets.length);

		if(((MapEntry<K, V>) this.buckets[apparentPosition]).getState() == true) {
			if(((MapEntry<K, V>) this.buckets[apparentPosition]).getKey().equals(k)) {
				V value = this.get(k);
				((MapEntry<K, V>) this.buckets[apparentPosition]).setKey(null);
				((MapEntry<K, V>) this.buckets[apparentPosition]).setValue(null);
				((MapEntry<K, V>) this.buckets[apparentPosition]).setState(false);
				currentSize--;
				return value;
			}
		}

		else {
			int secondPosition = this.HashFunctionSecondary(k, this.buckets.length);
			if(((MapEntry<K, V>) this.buckets[apparentPosition]).getState() == true) {
				if(((MapEntry<K, V>) this.buckets[secondPosition]).getKey().equals(k)) {
					V value = this.get(k);
					((MapEntry<K, V>) this.buckets[apparentPosition]).setKey(null);
					((MapEntry<K, V>) this.buckets[apparentPosition]).setValue(null);
					((MapEntry<K, V>) this.buckets[apparentPosition]).setState(false);
					currentSize--;
					return value;
				}
			}

			else {
				for (int i = 0; i < buckets.length; i++) {
					int position = (apparentPosition + i) % this.buckets.length;
					if(((MapEntry<K, V>) this.buckets[apparentPosition]).getState() == true) {
						if(((MapEntry<K, V>) this.buckets[position]).getKey().equals(k)) {
							V value = this.get(k);
							((MapEntry<K, V>) this.buckets[position]).setKey(null);
							((MapEntry<K, V>) this.buckets[position]).setValue(null);
							((MapEntry<K, V>) this.buckets[apparentPosition]).setState(false);
							currentSize--;
							return value;
						}
					}
				}
			}
		}

		return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean contains(K k) {

		if(k == null) return false;

		//First hashing.
		int apparentPosition = this.HashFunction(k, this.buckets.length);

		if(((MapEntry<K, V>) this.buckets[apparentPosition]).getState() == true) {
			//If the key in the bucket matches the given return true.
			if(((MapEntry<K, V>) this.buckets[apparentPosition]).getKey().equals(k)) {
				return true;
			}
		}

		else {

			//Second hashing.
			int secondPosition = this.HashFunctionSecondary(k, this.buckets.length);

			if(((MapEntry<K, V>) this.buckets[apparentPosition]).getState() == true) {
				//If the second hashing suceeds in finding the key, return true.
				if(((MapEntry<K, V>) this.buckets[secondPosition]).getKey() == k) {
					return true;
				}
			}

			//If the second hashing did not work, do linear probing.
			else {

				for (int i = 0; i < buckets.length; i++) {
					int position = (apparentPosition + i) % this.buckets.length;
					if(((MapEntry<K, V>) this.buckets[apparentPosition]).getState() == true) {
						if(((MapEntry<K, V>) this.buckets[position]).getKey().equals(k)) {
							return true;
						}
					}
				}
			}
		}

		//If none of the above succeeded, return false;
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SortedList<K> getKeys() {
		CircularSortedDoublyLinkedList<K> result = new CircularSortedDoublyLinkedList<K>(this.keyComp);

		for (int i = 0; i < buckets.length; i++) {
			if(((MapEntry<K, V>)this.buckets[i]).getState() == true) {
				result.add(((MapEntry<K, V>)this.buckets[i]).getKey());
			}
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public SortedList<V> getValues(){
		CircularSortedDoublyLinkedList<V> result = new CircularSortedDoublyLinkedList<V>(this.valueComp);
		for (int i = 0; i < buckets.length; i++) {
			if(((MapEntry<K, V>)this.buckets[i]).getState() == true) {
				result.add(((MapEntry<K, V>)this.buckets[i]).getValue());
			}
		}
		return result;
	}



}
