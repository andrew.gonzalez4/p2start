//IMPORTS
package edu.uprm.cse.datastructures.cardealer.model;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;  

@Path("/cars")
public class CarManager {

	
	private HashTableOA<Long,Car> cMap = CarTable.getInstance(); 
	//Get Method
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		SortedList<Car> l = cMap.getValues();
		Car[] result = new Car[l.size()];
		for (int i = 0; i < l.size(); i++) {
			result[i] = l.get(i);
		}
		
		return result;
	} 
	//Get Method with the ID
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		Car result = cMap.get(id);
		
		if(result == null) {
			throw new NotFoundException();
		}
		else {
			return result;
		}
	}      
	
	//Adding a car
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car obj){
		
		if(cMap.contains(obj.getCarId())) {
			return Response.status(Response.Status.CONFLICT).build();
		}
		cMap.put(obj.getCarId(), obj);
		return Response.status(201).build();
	}
	//Updating a car
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCustomer(Car obj){
		
		if(cMap.contains(obj.getCarId())) {
			cMap.remove(obj.getCarId());
			cMap.put(obj.getCarId(), obj);
			return Response.status(200).build();
		}
		
		else {
			return Response.status(404).build();
		}
	}     
	//Delete
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		
		if(cMap.contains(id)) {
			cMap.remove(id);
			return Response.status(Response.Status.OK).build();
		}
		//If you didn't found a car with the ID return not found.
		return Response.status(Response.Status.NOT_FOUND).build(); 

	}
}      


