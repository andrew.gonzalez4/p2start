package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	
	public Iterator<E> iterator(int index) {		
		return new CircularSortedDoublyLinkedListIterator<E>(index);
	}

	@Override
	public Iterator<E> iterator() {		
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	//Iterator in order to use the for each in the Car Manager Class.
	private class  CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		private Node<E> previousNode;
		private int indx = -1;

		public  CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
			this.indx = -1;

		}
		public  CircularSortedDoublyLinkedListIterator(int index) {
			this.nextNode = (Node<E>) header.getNext();
			this.indx = index;

		}
		@Override
		public boolean hasNext() {
			return nextNode.element != null;
		}

		public boolean hasPrev() {
			return previousNode.element != null;
		}
		@Override
		public E next() {
			if (this.hasNext() && indx < 0) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else if(this.hasNext() && indx  > -1) {
				int count = 0;
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				while(count < indx) {
					this.nextNode = this.nextNode.getNext();
					count++;
				}
				result = nextNode.getElement();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}


	}

	//Declarated the previous and next nodes of the header. 
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		public Node<E> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}
		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}


	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> comparator;

	//the header points to itself when empty since the list is circular.
	public  CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.comparator = cmp;
		this.header = new Node<>();
		this.currentSize = 0;
		this.comparator = cmp;
		this.header.setNext(this.header);
		this.header.setPrevious(this.header);
	}
	//for the add, since the list is sorted we first check if the list is empty, if so then just add the element without worrying.
	@Override
	public boolean add(E obj) {
		
		//first check if the obj is null, if so don't add the element.
		if(obj == null) {
			return false;
		}
		//if the list is empty, just add the element.
		Node<E> newNode = new Node<E>();
		newNode.setElement(obj);
		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.header.setPrevious(newNode);
			newNode.setNext(this.header);
			newNode.setPrevious(this.header);
			this.currentSize++;
			return true;
		}
		
		//for the next cases, we have to check where the element should be placed since the list is sorted.

		Node<E> temp = this.header.getNext();
		newNode.setElement(obj);
		while(true) {
			
			//if the elements are the same, just add 
			if(this.comparator.compare(temp.getElement(), newNode.getElement()) == 0) {
				newNode.setPrevious(temp.getPrevious());
				temp.getPrevious().setNext(newNode);
				temp.setPrevious(newNode);
				newNode.setNext(temp);
				this.currentSize++;
				return true;
			}
			
			//if the new element is greater than the temp, add it after taking into account the next is null, then use the header.prev.
			else if(comparator.compare(temp.getElement(), newNode.getElement()) <0 &&
					temp.getNext().getElement() == null) {

				newNode.setNext(header);
				this.header.setPrevious(newNode);
				temp.setNext(newNode);
				newNode.setPrevious(temp);
				this.currentSize++;
				return true;
			}
			
			//if the previous of the temp is null and the temp is greater than the new element, add it 
			else if(temp.getPrevious().getElement() == null && comparator.compare(temp.getElement(), newNode.getElement())>0) {

				newNode.setNext(temp);
				temp.setPrevious(newNode);
				header.setNext(newNode);
				newNode.setPrevious(header);
				this.currentSize++;
				return true;
			}

			//if the new node is greater than the temp and the next of the temp is greater than the new node, add in between
			else if(comparator.compare(temp.getElement(), newNode.getElement()) < 0  &&
					comparator.compare(temp.getNext().getElement(), newNode.getElement()) >0) {

				newNode.setNext(temp.getNext());
				temp.getNext().setPrevious(newNode);
				temp.setNext(newNode);
				newNode.setPrevious(temp);
				this.currentSize++;
				return true;
			}
			
			//if the next of the temp is null then add it after the temp
			else if(temp.getNext().getElement() == null) {
				temp.setNext(newNode);
				newNode.setPrevious(temp);
				newNode.setNext(this.header);
				this.header.setPrevious(newNode);
				this.currentSize++;
				return true;
			}


			//continue iterating if no if were meet
			temp = temp.getNext();

		}
	}

	@Override
	public int size() {
		//Just return the current size of the list
		return this.currentSize;
	}

	@Override
	
	//For the remove we have to take away the element after finding it and then rearranging the list by modifying the nodes.
	public boolean remove(E obj) {
		
		//If the object is null, you can't remove anything
		if(obj == null) {
			return false;
		}
		
		//Set a temp variable to iterate
		Node<E> temp = this.header.getNext();

		while(true) { 
			
			//If the temp is equal to the object then remove and arrange the nodes how they are supposed to
			if(this.comparator.compare(temp.getElement(), obj) == 0) {
				temp.getPrevious().setNext(temp.getNext());
				temp.getNext().setPrevious(temp.getPrevious());
				temp.setNext(null);
				temp.setPrevious(null);
				temp.setElement(null);
				this.currentSize--;
				return true; 
			}
			
			//If the next is null, it means you reached the end and the element doesnt exist so return false.
			else if(temp.getNext().getElement() == null) {
				return false;
			}
			temp = temp.getNext();
		}


	} 

	//To remove by index we need to check where the element is by iterating with a temp
	@Override
	public boolean remove(int index) {
		//If the index is <0 of greater than the size then you cannot remove an index which doesn't exist.
		if(index < 0 || this.size() <= index) {
			throw new IndexOutOfBoundsException();
		}
		//Create a temp to iterate until you get to the index
		Node<E> temp = this.header.getNext();
		while(index != 0) {
			temp = temp.getNext();
			index--;
		}

		//When you are on the index, remove and organize the nodes respectively.
		temp.getPrevious().setNext(temp.getNext());
		temp.getNext().setPrevious(temp.getPrevious());
		temp.setNext(null);
		temp.setPrevious(null);
		temp.setElement(null);
		this.currentSize--;
		return true; 
	}

	//For the remove all just create a variable and add to it everytime you delete a copy of the object.
	@Override
	public int removeAll(E obj) { 
		int count = 0;
		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	//Just return the first elemennt after the header.
	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	//Return the element before the header since the list is circular.
	@Override 
	public E last() {
		return this.header.getPrevious().getElement(); 
	}

	//To get an element with index one has to traverse with an iterator(temp).
	@Override
	public E get(int index) {
		//If the index is less than 0 or the index is greater than the size, it doesn't exist that index.
		if(index < 0 || this.size() <= index) {
			throw new IndexOutOfBoundsException();
		}
		//Temp node to traverse the list.
		Node<E> result = this.header;
		//Continue iterating until the index is reached.
		for (int i = 0; i <= index; i++) {
			result = result.getNext();
		}
		//Return the element at that index.
		return result.getElement();
	}

	//Just keep removing index 0 while the list is not Empty.
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}

	}

	//To see if a list contains an object just check if there is a first index of that object.
	@Override
	public boolean contains(E e) {

		if(this.firstIndex(e)!= -1) {
			return true;
		}

		return false;

	}

	//If the size is 0 then the list is empty.
	@Override
	public boolean isEmpty() {
		if(this.size() == 0) {
			return true;
		}
		return false;
	}
	
	// To get the first index of an element one must iterate to find it.
	@Override
	public int firstIndex(E e) {
		//Iterator temp
		Node<E> temp = this.header.getNext();
		int count = 0;
		//Keep iterating until you find the element with the comparator or until you see the next is null.
		while(true) {

			if(this.comparator.compare(temp.getElement(), e) == 0) {
				return count;
			}
			else if(temp.getNext().getElement() == null) {
				return -1;
			} 
			count++;
			temp = temp.getNext();
		}


	}

	//Same as last time, just that instead of going forward you go backwards.
	@Override
	public int lastIndex(E e) {
		Node<E> temp = this.header.getPrevious();
		int count = this.currentSize - 1;
		while(true) {

			if(this.comparator.compare(temp.getElement(), e) == 0) {
				return count;
			}
			else if(temp.getPrevious().getElement() == null) {
				return -1;
			} 
			count--;
			temp = temp.getPrevious();
		}
	}

}